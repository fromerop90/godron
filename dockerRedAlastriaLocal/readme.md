# Red Local de Alastria en Docker

He creado una imagen ([hub.docker.com](https://hub.docker.com/repository/docker/fcoromeroporras/ubuntualastria/general)) de contenedor para levantar una red local de Alastria facilmente.
# Files
Dockerfile
start_node.sh ( He modificado el fichero para añadir al servidor Geth la variable RPC_CORS_DOMAIN="https://localhost*" )

## Personalizar
Se podrá personalizar el fichero alastriaInit.sh para crear una red con mas o menos nodos.

## Instalar Docker
Seguir la documentación oficial para instalar Docker [https://docs.docker.com/get-docker/](https://docs.docker.com/get-docker/)

## Generar imagen personalizada a partir del Dockerfile.
docker build -t img-red-alastria ./dockerRedAlastriaLocal

## Iniciarmos el contendor
docker run \
--name dk-eviroment-alastria \
-p 3000:3000 \
-it img-red-alastria

Podemos salir escribiendo:

exit

## Encendemos/Apagamos el contenedor
docker start dk-eviroment-alastria

docker stop dk-eviroment-alastria

## Accedemos al contenedor encedido
docker exec -it dk-eviroment-alastria bash


## Arrancamos la red con 1 validador y 2 regulares
sudo ./bin/start_network.sh clean 1 2

## Arrancamos el monitor de red
sudo ./bin/start_ethstats.sh 

## Conectamos al nodo general1
sudo geth attach ipc:network/general1/geth.ipc
# Visualizamos las cuentas
web3.eth.accounts
# Desbloqueamos la cuenta de admin
web3.personal.unlockAccount(web3.eth.accounts[0], "Passw0rd",0);

# Puertos
Se han habilitado los puertos:
-   **3000**: Puerto por el se puede acceder al acceder al visualizador de estadísticas: [http://localhost:3000/](http://localhost:3000/)
