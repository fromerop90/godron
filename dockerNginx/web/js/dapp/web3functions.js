var web3 = new Web3(new Web3.providers.HttpProvider("https://localhost:22001"));

async function lockAccount(account) {
	console.log("lockAccount Empieza");
	account = account.toLowerCase();
	const isLockAccount = await web3.eth.personal.lockAccount(account);
	if (isLockAccount) {
		console.log('Account locked!')
	} else {
		console.log('Account non locked!')
	}
	console.log("lockAccount Termina");
	return isLockAccount;
}

async function unlockAccount(account, password) {
	console.log("unlockAccount Empieza");
	account = account.toLowerCase();
	const isUnlockAccount = await web3.eth.personal.unlockAccount(account, password, 600);
	if (isUnlockAccount) {
		console.log('Account unlocked!')
	} else {
		console.log(error);
		console.log('Account non unlocked!');
		alert("No ha inciado sesión");
	}
	console.log("unlockAccount Termina");
	return isUnlockAccount;
}

async function isAdmin(user) {
	console.log("isAdmin Empieza");
	const accounts = await web3.eth.getAccounts();
	console.log("isAdmin Termina");
	return accounts[0].toLowerCase() == user.toLowerCase() && user.toLowerCase() == admin_address.toLowerCase();
}


async function checkAdminAccess() {
	console.log("checkAdminAccess Empieza");
	checkUserAccess();
	var user = sessionStorage.getItem("sessionUser");
	user = user.toLowerCase();
	const isAdminBool = await isAdmin(user);
	if (!isAdminBool) {
			alert("Acceso Restringido.")
			lockAccount(user)
			window.location = 'login.html';
	}
	console.log("checkAdminAccess Termina");
}


async function checkUserAccess() {
	console.log("checkUserAccess Empieza");
	var user = sessionStorage.getItem("sessionUser");
	if (user == null) {
		alert("Acceso Restringido.")
		window.location = 'login.html';
	} 
	console.log("checkUserAccess Termina");
}



