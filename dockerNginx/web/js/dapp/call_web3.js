//Se instancia el objeto web3
var web3 = new Web3(new Web3.providers.HttpProvider("https://localhost:22001"));


//async function start() {
//	// Gett all the accounts
//	const accounts = await web3.eth.getAccounts();
//	alert(accounts);
//	console.log(accounts);
//}
//
//start();

async function asyncGetAccounts() {
	console.log("asyncGetAccounts Empieza");
	const accounts = await web3.eth.getAccounts();
	alert(accounts);
	console.log(accounts);
	console.log("asyncGetAccounts Termina");
}

function getAccounts() {
	console.log("getAccounts Empieza");
	web3.eth.personal.getAccounts()
		.then(console.log);
	console.log("getAccounts Termina");
}

async function asyncNewAccount(password) {
	console.log("asyncNewAccount Empieza");
	const newAccount = await web3.eth.personal.newAccount(password);
	alert(newAccount);
	console.log(newAccount);
	console.log("asyncNewAccount Termina");
}

function newAccount(password) {
	console.log("newAccount Empieza");
	web3.eth.personal.newAccount(password)
.		then(console.log);
	console.log("newAccount Termina");
}


async function asyncUnlockAccount(account, password) {
	console.log("asyncUnlockAccount Empieza");
	const unlockAccount = await web3.eth.personal.unlockAccount(account, password, 600);
	alert(unlockAccount);
	console.log(unlockAccount);
	console.log("asyncUnlockAccount Termina");
}


function unlockAccount(account, password) {
	console.log("unlockAccount Empieza");
	web3.eth.personal.unlockAccount(account, password, 600)
		.then(console.log('Account unlocked!'));
	console.log("unlockAccount Termina");
}

async function asyncLockAccount(account) {
	console.log("asyncLockAccount Empieza");
	const lockAccount = await web3.eth.personal.lockAccount(account);
	alert(lockAccount);
	console.log(lockAccount);
	console.log("asyncLockAccount Termina");
}

function lockAccount(account) {
	console.log("lockAccount Empieza");
	web3.eth.personal.lockAccount(account)
		.then(console.log('Account locked!'));
	console.log("lockAccount Termina");
}


function test() {
	console.log("Test Empieza");
	var password = "Passw0rd";
	getAccounts();
//	lockAccount("0x74d4C56d8dcbC10A567341bFac6DA0A8F04DC41d");
//	unlockAccount("0x74d4C56d8dcbC10A567341bFac6DA0A8F04DC41d", password);
//	newAccount(password);
	console.log("Test Termina");
}

function asyncTest() {
	console.log("asyncTest Empieza");
	var password = "Passw0rd";
	asyncGetAccounts();
	asyncLockAccount("0x74d4C56d8dcbC10A567341bFac6DA0A8F04DC41d");
	asyncUnlockAccount("0x74d4C56d8dcbC10A567341bFac6DA0A8F04DC41d", password);
	asyncNewAccount(password);
	console.log("asyncTest Termina");
}

test();
//asyncTest();