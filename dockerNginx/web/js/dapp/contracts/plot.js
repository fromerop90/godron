var plotContract;

function startPlotContract() {
	if (plotContract === undefined || plotContract.methods === undefined) {
		$.ajax({
			url: "./js/dapp/abi/Plot.json",
			dataType: 'json',
			async: false,
			success: function(ABI_DRONE) {
				plotContract = new web3.eth.Contract(ABI_DRONE, dir_plot);
				console.log(plotContract);
			}
		});
	}
}

startPlotContract();

async function getPlotOwner() {
	console.log("getPlotOwner Empieza");
	await checkUserAccess();
	plotContract.methods.owner().call(function(error, result) {
		if (!error) {
			console.log("plotOwner:" + result);
			console.log("getPlotOwner Termina");
			return result;
		} else {
			alert(error);
			console.error(error);
			return null;
		}
	});
}



async function createNewTokenPlot(minFlyHeight, maxFlyHeight, presticideIndex) {
	alert("Se va a crear un nuevo Token, por favor, espere...");
	console.log("createNewTokenPlot Empieza");
	await checkUserAccess();
	if (presticideIndex == null || presticideIndex == "" || presticideIndex < 0 || presticideIndex > presticidesTotalList.length) {
		alert("Error: El pesticida permitido es erroneo.")
	} else {
		var currentPest = presticidesTotalList[presticideIndex];
		if (minFlyHeight > maxFlyHeight) {
			alert("Error: La altura maxima de vuelto, tiene que ser mayor que la altura minima de vuelo")
		} else {
			var user = sessionStorage.getItem("sessionUser");
			user = user.toLowerCase();

			startPlotContract();

			plotContract.methods.createNewToken(user, minFlyHeight, maxFlyHeight, currentPest).send({ from: admin_address, gas: 30000000 }).on('receipt', function(receipt) {
				if (receipt.events.createNewTokenPlot) {
					var evento = receipt.events.createNewTokenPlot;
					console.log(evento);
					var tokenId = evento.returnValues[0];
					alert("Creación de Token Exitosa con ID: " + tokenId);
					getPlotTotalSupply();
					resetPlotInputForm();
				} else {
					alert("Error en Creación de Token");
				}
			}).catch(function(error) {
				alert("Error en Creación de Token");
				console.log(error);
			});
		}
	}
	console.log("createNewTokenPlot Termina");
}


async function getPlotTotalSupply() {
	console.log("getPlotTotalSupply Empieza");
	await checkUserAccess();

	startPlotContract();
	plotContract.methods.totalSupply().call(function(error, result) {
		if (!error) {
			console.log("totalSupply:" + result);
			refreshPlotDataGrid(result);
			console.log("getPlotTotalSupply Termina");
			return result;
		} else {
			alert(error);
			console.error(error);
			return null;
		}
	});
}

async function getPlotMinFlyHeight(tokenId) {
	console.log("getPlotMinFlyHeight Empieza");
	await checkUserAccess();

	startPlotContract();

	plotContract.methods.getMinFlyHeight(tokenId).call(function(error, result) {
		if (!error) {
			console.log("getPlotMinFlyHeight:" + result);
			document.getElementById("minFlyHeight_" + tokenId).value = result;
			console.log("getPlotMinFlyHeight Termina");
			return result;
		} else {
			alert(error);
			console.error(error);
			return null;
		}
	});
}

async function getPlotMaxFlyHeight(tokenId) {
	console.log("getPlotMaxFlyHeight Empieza");
	await checkUserAccess();

	startPlotContract();

	plotContract.methods.getMaxFlyHeight(tokenId).call(function(error, result) {
		if (!error) {
			console.log("getPlotMaxFlyHeight:" + result);
			document.getElementById("maxFlyHeight_" + tokenId).value = result;
			console.log("getPlotMaxFlyHeight Termina");
			return result;
		} else {
			alert(error);
			console.error(error);
			return null;
		}
	});
}

async function getPlotAvailablePesticide(tokenId) {
	console.log("getPlotAvailablePesticide Empieza");
	await checkUserAccess();

	startPlotContract();

	plotContract.methods.getAvailablePesticide(tokenId).call(function(error, result) {
		if (!error) {
			console.log("getPlotAvailablePesticide:" + result);
			for (var i = 0; i <= presticidesTotalList.length; i++) {
				if (presticidesTotalList[i].toUpperCase() == result.toUpperCase()) {
					document.getElementById("availablePesticide_" + tokenId).value = i;
					console.log("getPlotCost Termina");
					return result;
				}
			}
			return null;
		} else {
			alert(error);
			console.error(error);
			return null;
		}
	});
}

async function getOwnerPlotAddress(tokenId) {
	console.log("getOwnerPlotAddress Empieza");
	await checkUserAccess();

	startPlotContract();

	plotContract.methods.getOwnerPlotAddress(tokenId).call(function(error, result) {
		if (!error) {
			console.log("getOwnerPlotAddress:" + result);
			var user = sessionStorage.getItem("sessionUser");
			user = user.toLowerCase();
			if (user.toLowerCase() == result.toLowerCase()) {
				writeLinePlotDataGrid(tokenId)
			}
			console.log("getOwnerPlotAddress Termina");
			return result;
		} else {
			alert(error);
			console.error(error);
			return null;
		}
	});
}

async function setMinFlyHeightPlot(tokenId, minFlyHeight) {
	console.log("setMinFlyHeightPlot Empieza");
	alert("Se va a actualizar Token con ID: " + tokenId + " con minFlyHeight: " + minFlyHeight + ", por favor, espere...");
	await checkUserAccess();
	var user = sessionStorage.getItem("sessionUser");
	user = user.toLowerCase();

	startPlotContract();

	plotContract.methods.setMinFlyHeight(tokenId, minFlyHeight).send({ from: admin_address, gas: 30000000 }).on('receipt', function(receipt) {
		if (receipt.events.updateTokenPlot) {
			var evento = receipt.events.updateTokenPlot;
			console.log(evento);
			alert("Actualización de Token Exitosa con ID: " + evento.returnValues[0]);
			document.getElementById("minFlyHeight_" + tokenId).value = minFlyHeight;
		} else {
			alert("Error en Actualización de Token");
		}
	}).catch(function(error) {
		console.log(error);
		alert("Error en Actualización de Token");
	});
	console.log("setMinFlyHeightPlot Termina");
}

async function setMaxFlyHeightPlot(tokenId, maxFlyHeight) {
	console.log("setMaxFlyHeightPlot Empieza");
	alert("Se va a actualizar Token con ID: " + tokenId + " con maxFlyHeight: " + maxFlyHeight + ", por favor, espere...");
	await checkUserAccess();
	var user = sessionStorage.getItem("sessionUser");
	user = user.toLowerCase();

	startPlotContract();

	plotContract.methods.setMaxFlyHeight(tokenId, maxFlyHeight).send({ from: admin_address, gas: 30000000 }).on('receipt', function(receipt) {
		if (receipt.events.updateTokenPlot) {
			var evento = receipt.events.updateTokenPlot;
			console.log(evento);
			alert("Actualización de Token Exitosa con ID: " + evento.returnValues[0]);
			document.getElementById("maxFlyHeight_" + tokenId).value = maxFlyHeight;
		} else {
			alert("Error en Actualización de Token");
		}
	}).catch(function(error) {
		console.log(error);
		alert("Error en Actualización de Token");
	});
	console.log("setMaxFlyHeightPlot Termina");
}

async function setAvailablePesticidePlot(tokenId, presticideIndex) {
	console.log("setAvailablePesticidePlot Empieza");

	if (presticideIndex != null && presticideIndex < 0 && presticideIndex > presticidesTotalList.length) {
		alert("Error: El pesticida permitido es erroneo.")
	} else {
		var currentPest = presticidesTotalList[presticideIndex];
		alert("Se va a actualizar Token con ID: " + tokenId + " con availablePesticide: " + currentPest + ", por favor, espere...");
		await checkUserAccess();
		var user = sessionStorage.getItem("sessionUser");
		user = user.toLowerCase();

		startPlotContract();

		plotContract.methods.setAvailablePesticide(tokenId, currentPest).send({ from: admin_address, gas: 30000000 }).on('receipt', function(receipt) {
			if (receipt.events.updateTokenPlot) {
				var evento = receipt.events.updateTokenPlot;
				console.log(evento);
				alert("Actualización de Token Exitosa con ID: " + evento.returnValues[0]);
				document.getElementById("availablePesticide_" + tokenId).value = presticideIndex;
			} else {
				alert("Error en Actualización de Token");
			}
		}).catch(function(error) {
			console.log(error);
			alert("Error en Actualización de Token");
		});
	}

	console.log("setAvailablePesticidePlot Termina");
}
