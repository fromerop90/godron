var airCoinContract;

function startAirCoinContract() {
	if (airCoinContract === undefined || airCoinContract.methods === undefined) {
		$.ajax({
			url: "./js/dapp/abi/AirCoin.json",
			dataType: 'json',
			async: false,
			success: function(ABI_AIR_COIN) {
				airCoinContract = new web3.eth.Contract(ABI_AIR_COIN, dir_airCoin);
				console.log(airCoinContract);
			}
		});
	}
}

startAirCoinContract();

async function getAirCoinTotalSupply() {
	console.log("getAirCoinTotalSupply Empieza");
	await checkAdminAccess();

	startAirCoinContract();

	airCoinContract.methods.totalSupply().call(function(error, result) {
		if (!error) {
			console.log("totalSupply:" + result);
			console.log("getAirCoinTotalSupply Termina");
			document.getElementById('totalSupplyValue').innerHTML = result;
			return result;
		} else {
			alert(error);
			console.error(error);
			return null;
		}
	});
}

async function getAirCoinOwner() {
	console.log("getAirCoinOwner Empieza");
	await checkAdminAccess();

	startAirCoinContract();

	airCoinContract.methods.owner().call(function(error, result) {
		if (!error) {
			console.log("airCoinOwner:" + result);
			console.log("getAirCoinOwner Termina");
			return result;
		} else {
			alert(error);
			console.error(error);
			return null;
		}
	});
}

async function getAirCoinBalanceOf(addressAccount) {
	console.log("getAirCoinOwner Empieza");
	await checkAdminAccess();
	addressAccount = addressAccount.toLowerCase();
	airCoinContract.methods.balanceOf(addressAccount).call(function(error, result) {
		if (!error) {
			console.log("balanceOf:" + result);
			console.log("getAirCoinOwner Termina");
			document.getElementById("balance_" + addressAccount).value = result;
			return result;
		} else {
			alert(error);
			console.error(error);
			return null;
		}
	});
}

async function createNewTokenAirCoin(amount) {
	alert("Se va a crear un nuevo Token, por favor, espere...");
	console.log("createNewTokenAirCoin Empieza");
	await checkAdminAccess();
	var user = sessionStorage.getItem("sessionUser");
	user = user.toLowerCase();

	startAirCoinContract();


	airCoinContract.methods.createNewToken(amount).send({ from: user, gas: 30000000 }).on('receipt', function(receipt) {
		if (receipt.events.createNewTokenAirCoin) {
			var evento = receipt.events.createNewTokenAirCoin;
			alert("Creación de Token Exitosa");
			getAirCoinTotalSupply();
			getAirCoinBalanceOf(user);
			document.getElementById("valueAddAirCoin").value = null;
		} else {
			alert("Error en Creación de Token");
		}
	}).catch(function(error) {
		console.log(error);
		alert("Error en Creación de Token");
	});
	console.log("createNewTokenAirCoin Termina");
}



async function transferAirCoin(addressTo, amount) {
	alert("Se van a transferir " + amount + " AIR a " + addressTo + ", por favor, espere...");
	console.log("transferAirCoin Empieza");
	await checkAdminAccess()
	var user = sessionStorage.getItem("sessionUser");
	user = user.toLowerCase();
	addressTo = addressTo.toLowerCase();

	startAirCoinContract();


	airCoinContract.methods.transfer(addressTo, amount).send({ from: user, gas: 30000000 }).on('receipt', function(receipt) {
		if (receipt.events.Transfer) {
			var evento = receipt.events.Transfer;
			console.log(evento);
			alert("Transfetencia Exitosa");
			getAirCoinBalanceOf(addressTo);
			getAirCoinBalanceOf(user);
		} else {
			alert("Error en Transfetencia");
		}
	}).catch(function(error) {
		console.log(error);
		alert("Error en Transfetencia");
	});
	console.log("transferAirCoin Termina");
}