var goDronServiceContract;

function startGoDronServiceContract() {
	if (goDronServiceContract === undefined || goDronServiceContract.methods === undefined) {
		$.ajax({
			url: "./js/dapp/abi/GoDronService.json",
			dataType: 'json',
			async: false,
			success: function(ABI_GO_DRON_SERVICE) {
				goDronServiceContract = new web3.eth.Contract(ABI_GO_DRON_SERVICE, dir_goDronService);
				console.log(goDronServiceContract);
			}
		});
	}
}

startGoDronServiceContract();

async function getGoDronServiceOwner() {
	console.log("getGoDronServiceOwner Empieza");
	await checkAccess();
	goDronServiceContract.methods.owner().call(function(error, result) {
		if (!error) {
			console.log("goDronServiceOwner:" + result);
			console.log("getGoDronServiceOwner Termina");
			return result;
		}
		else
			console.error(error);
		return null;
	});
}

async function rentDrone(droneTokenId) {
	console.log("rentDrone Empieza");
	alert("Comienza Alquiler del Dron " + droneTokenId + ", por favor, espere.");
	await checkUserAccess();
	var managementCompanyAddress = sessionStorage.getItem("sessionUser");
	managementCompanyAddress = managementCompanyAddress.toLowerCase();

	startDroneContract();
	goDronServiceContract.methods.rentDrone(droneTokenId, managementCompanyAddress).send({ from: admin_address, gas: 300000000 }).on('receipt', function(receipt) {
		if (receipt.events.DroneRented) {
			var evento = receipt.events.DroneRented;
			console.log(evento);
			alert("Alquiler del Dron " + droneTokenId + " por el cliente " + managementCompanyAddress + " Exitoso");
			getDroneTotalSupply();
			getCurrentUserBalance();
		} else {
			alert("Error en Alquiler del Dron");
		}
	}).catch(function(error) {
		console.log(error);
		alert("Error en Alquiler del Dron");
	});
	console.log("rentDrone Termina");
}


async function fumigate(droneTokenId, plotTokenId) {
	console.log("fumigate Empieza");
			alert("Comienza Fumigación de Parcela" + plotTokenId + " por el dron " + droneTokenId + ", por favor, espere.");
	await checkUserAccess();

	startDroneContract();
	goDronServiceContract.methods.fumigate(droneTokenId, plotTokenId).send({ from: admin_address, gas: 300000000 }).on('receipt', function(receipt) {
		if (receipt.events.PlotFumigated) {
			var evento = receipt.events.PlotFumigated;
			console.log(evento);
			alert("Fumigación de Parcela" + plotTokenId + " por el dron " + droneTokenId + " Exitosa");
			getDroneTotalSupply();
		} else {
			alert("Error en Fumigación de Parcela");
		}
	}).catch(function(error) {
		console.log(error);
		alert("Error en Fumigación de Parcela");
	});
	console.log("fumigate Termina");
}