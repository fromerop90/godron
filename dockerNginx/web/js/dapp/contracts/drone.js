var droneContract;

function startDroneContract() {
	if (droneContract === undefined || droneContract.methods === undefined) {
		$.ajax({
			url: "./js/dapp/abi/Drone.json",
			dataType: 'json',
			async: false,
			success: function(ABI_DRONE) {
				droneContract = new web3.eth.Contract(ABI_DRONE, dir_drone);
				console.log(droneContract);
			}
		});
	}
}

startDroneContract();

async function getDroneOwner() {
	console.log("getDroneOwner Empieza");
	await checkUserAccess();
	startDroneContract();

	droneContract.methods.owner().call(function(error, result) {
		if (!error) {
			console.log("droneOwner:" + result);
			console.log("getDroneOwner Termina");
			return result;
		} else {
			alert(error);
			console.error(error);
			return null;
		}
	});
}


async function createNewTokenDrone(minFlyHeight, maxFlyHeight, cost) {
	console.log("createNewTokenDrone Empieza");
	alert("Se va a crear un nuevo Token, por favor, espere...");
	await checkAdminAccess();
	if (minFlyHeight > maxFlyHeight) {
		alert("Error: La altura maxima de vuelto, tiene que ser mayor que la altura minima de vuelo")
	} else {
		if (cost < 0) {
			alert("Error: El coste del alquiler no puede ser negativo")
		} else {
			var user = sessionStorage.getItem("sessionUser");
			user = user.toLowerCase();
			startDroneContract();

			droneContract.methods.createNewToken(minFlyHeight, maxFlyHeight, cost).send({ from: user, gas: 30000000 }).on('receipt', function(receipt) {
				if (receipt.events.createNewTokenDrone) {
					var evento = receipt.events.createNewTokenDrone;
					console.log(evento);
					var tokenId = evento.returnValues[0];
					alert("Creación de Token Exitosa con ID: " + tokenId);
					getDroneTotalSupply();
					resetDroneInputForm();
				} else {
					alert("Error en Creación de Token");
				}
			}).catch(function(error) {
				console.log(error);
				alert("Error en Creación de Token");
			});
		}
	}
	console.log("createNewTokenDrone Termina");
}


async function getDroneTotalSupply() {
	console.log("getDroneTotalSupply Empieza");
	await checkUserAccess();

	startDroneContract();

	droneContract.methods.totalSupply().call(function(error, result) {
		if (!error) {
			console.log("totalSupply:" + result);
			refreshDroneDataGrid(result);
			console.log("getDroneTotalSupply Termina");
			return result;
		} else {
			alert(error);
			console.error(error);
			return null;
		}
	});
}

async function getDroneMinFlyHeight(tokenId) {
	console.log("getDroneMinFlyHeight Empieza");
	await checkUserAccess();

	startDroneContract();

	droneContract.methods.getMinFlyHeight(tokenId).call(function(error, result) {
		if (!error) {
			console.log("getDroneMinFlyHeight:" + result);
			document.getElementById("minFlyHeight_" + tokenId).value = result;
			console.log("getDroneMinFlyHeight Termina");
			return result;
		} else {
			alert(error);
			console.error(error);
			return null;
		}
	});
}

async function getDroneMaxFlyHeight(tokenId) {
	console.log("getDroneMaxFlyHeight Empieza");
	await checkUserAccess();

	startDroneContract();

	droneContract.methods.getMaxFlyHeight(tokenId).call(function(error, result) {
		if (!error) {
			console.log("getDroneMaxFlyHeight:" + result);
			document.getElementById("maxFlyHeight_" + tokenId).value = result;
			console.log("getDroneMaxFlyHeight Termina");
			return result;
		} else {
			alert(error);
			console.error(error);
			return null;
		}
	});
}

async function getDroneCost(tokenId) {
	console.log("getDroneCost Empieza");
	await checkUserAccess();

	startDroneContract();

	droneContract.methods.getCost(tokenId).call(function(error, result) {
		if (!error) {
			console.log("getDroneCost:" + result);
			document.getElementById("cost_" + tokenId).value = result;
			console.log("getDroneCost Termina");
			return result;
		} else {
			alert(error);
			console.error(error);
			return null;
		}
	});
}

async function isAvailablePesticideDrone(tokenId, presticideIndex) {
	console.log("isAvailablePesticideDrone Empieza");
	await checkUserAccess();
	var currentPest = presticidesTotalList[presticideIndex];
	currentPest = currentPest.toUpperCase();

	startDroneContract();

	droneContract.methods.isAvailablePesticide(tokenId, currentPest).call(function(error, result) {
		if (!error) {
			console.log("isAvailablePesticideDrone:" + result);
			document.getElementById('pesticide_' + tokenId + '_' + presticideIndex).value = result;
			console.log("isAvailablePesticideDrone Termina");
			return result;
		} else {
			alert(error);
			console.error(error);
			return null;
		}
	});

}

async function getDroneReleaseTime(tokenId) {
	console.log("getDroneReleaseTime Empieza");
	await checkUserAccess();

	startDroneContract();

	droneContract.methods.getReleaseTime(tokenId).call(function(error, result) {
		if (!error) {
			console.log("getDroneReleaseTime:" + result);
			if (result > 0) {
				var fechaResult = new Date(0);
				fechaResult.setUTCSeconds(result);
				var currentDate = new Date();
				if (fechaResult > currentDate) {
					document.getElementById("releaseTime_" + tokenId).value = fecha.toLocaleString();
				} else {
					document.getElementById("releaseTime_" + tokenId).value = null;
				}
			} else {
				document.getElementById("releaseTime_" + tokenId).value = null;
			}
			console.log("getDroneReleaseTime Termina");
			return result;
		} else {
			alert(error);
			console.error(error);
			return null;
		}
	});
}

async function getDroneManagementCompany(tokenId) {
	console.log("getDroneReleaseTime Empieza");
	await checkUserAccess();

	startDroneContract();

	droneContract.methods.getManagementCompanyAddress(tokenId).call(function(error, result) {
		if (!error) {
			console.log("getDroneReleaseTime:" + result);
			if (result.toLowerCase() != admin_address.toLowerCase()) {
				document.getElementById("managementCompany_" + tokenId).value = result;
				console.log("getDroneReleaseTime Termina");
				return result;
			} else {
				console.log("getDroneReleaseTime Termina");
				return null;
			}
		} else {
			alert(error);
			console.error(error);
			return null;
		}
	});
}


async function setMinFlyHeightDrone(tokenId, minFlyHeight) {
	console.log("setMinFlyHeightDrone Empieza");
	alert("Se va a actualizar Token con ID: " + tokenId + " con minFlyHeight: " + minFlyHeight + ", por favor, espere...");
	await checkAdminAccess();
	var user = sessionStorage.getItem("sessionUser");
	user = user.toLowerCase();

	startDroneContract();

	droneContract.methods.setMinFlyHeight(tokenId, minFlyHeight).send({ from: user, gas: 30000000 }).on('receipt', function(receipt) {
		if (receipt.events.UpdateTokenDrone) {
			var evento = receipt.events.UpdateTokenDrone;
			console.log(evento);
			alert("Actualización de Token Exitosa con ID: " + evento.returnValues[0]);
			document.getElementById("minFlyHeight_" + tokenId).value = minFlyHeight;
		} else {
			alert("Error en Actualización de Token");
		}
	}).catch(function(error) {
		console.log(error);
		alert("Error en Actualización de Token");
	});
	console.log("setMinFlyHeightDrone Termina");
}

async function setMaxFlyHeightDrone(tokenId, maxFlyHeight) {
	console.log("setMaxFlyHeightDrone Empieza");
	alert("Se va a actualizar Token con ID: " + tokenId + " con maxFlyHeight: " + maxFlyHeight + ", por favor, espere...");
	await checkAdminAccess();
	var user = sessionStorage.getItem("sessionUser");
	user = user.toLowerCase();

	startDroneContract();

	droneContract.methods.setMaxFlyHeight(tokenId, maxFlyHeight).send({ from: user, gas: 30000000 }).on('receipt', function(receipt) {
		if (receipt.events.UpdateTokenDrone) {
			var evento = receipt.events.UpdateTokenDrone;
			console.log(evento);
			alert("Actualización de Token Exitosa con ID: " + evento.returnValues[0]);
			document.getElementById("maxFlyHeight_" + tokenId).value = maxFlyHeight;
		} else {
			alert("Error en Actualización de Token");
		}
	}).catch(function(error) {
		console.log(error);
		alert("Error en Actualización de Token");
	});
	console.log("setMaxFlyHeightDrone Termina");
}

async function setCostDrone(tokenId, cost) {
	console.log("setCostDrone Empieza");
	alert("Se va a actualizar Token con ID: " + tokenId + " con cost: " + cost + ", por favor, espere...");
	await checkAdminAccess();
	var user = sessionStorage.getItem("sessionUser");
	user = user.toLowerCase();

	startDroneContract();

	droneContract.methods.setCost(tokenId, cost).send({ from: user, gas: 30000000 }).on('receipt', function(receipt) {
		if (receipt.events.UpdateTokenDrone) {
			var evento = receipt.events.UpdateTokenDrone;
			console.log(evento);
			alert("Actualización de Token Exitosa con ID: " + evento.returnValues[0]);
			document.getElementById("cost_" + tokenId).value = cost;
		} else {
			alert("Error en Actualización de Token");
		}
	}).catch(function(error) {
		console.log(error);
		alert("Error en Actualización de Token");
	});
	console.log("setCostDrone Termina");
}

async function addPesticideDrone(tokenId, presticideIndex, boolValue) {
	console.log("setCostDrone Empieza");
	var currentPest = presticidesTotalList[presticideIndex];
	alert("Se va a actualizar Token con ID: " + tokenId + " con pesticida: " + currentPest + " disponible: " + boolValue + " , por favor, espere...");
	await checkAdminAccess();
	var user = sessionStorage.getItem("sessionUser");
	user = user.toLowerCase();

	startDroneContract();

	droneContract.methods.addPesticide(tokenId, currentPest, boolValue).send({ from: user, gas: 30000000 }).on('receipt', function(receipt) {
		if (receipt.events.UpdateTokenDrone) {
			var evento = receipt.events.UpdateTokenDrone;
			console.log(evento);
			alert("Actualización de Token Exitosa con ID: " + evento.returnValues[0]);
			document.getElementById('pesticide_' + tokenId + '_' + presticideIndex).value = boolValue;
		} else {
			alert("Error en Actualización de Token");
		}
	}).catch(function(error) {
		console.log(error);
		alert("Error en Actualización de Token");
	});
	console.log("setCostDrone Termina");
}
