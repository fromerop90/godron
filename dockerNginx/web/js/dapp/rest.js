
async function getRestAsync(url) {
	const userAction = async () => {
		const response = await fetch(url);
		const myJson = await response.json(); // extract JSON from the http
		// response
		// do something with myJson
		return myJson;
	};
}

async function postRestAsync(url, body) {
	const userAction = async () => {
		const response = await fetch(url, {
			method: 'POST',
			body: myBody, // string or object
			headers: {
				'Content-Type': 'application/json'
			}
		});
		const myJson = await response.json(); // extract JSON from the http
		// response
		// do something with myJson
		return myJson;
	};
}


function getRest(url) {
	getRestAsync(url).then(myJson => {
		console.log(myJson)
	});
}

function postRest(url, myBody) {
	postRestAsync(url, myBody).then(myJson => {
		console.log(myJson)
	});
}