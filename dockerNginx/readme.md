# Red Local de Alastria en Docker

He creado una imagen ([hub.docker.com](https://hub.docker.com/repository/docker/fcoromeroporras/ubuntualastria/general)) de contenedor para levantar una red local de Alastria facilmente.
# Files
Dockerfile
## Personalizar
Se podrá personalizar el el contenido de la web en la carpeta /web y la configuración de nginx en la carpeta /nginx

## Instalar Docker
Seguir la documentación oficial para instalar Docker [https://docs.docker.com/get-docker/](https://docs.docker.com/get-docker/)

## Generar imagen personalizada a partir del Dockerfile.
docker build -t img-nginx ./dockerNginx


## Iniciarmos el contendor

docker run \
--name dk-nginx \
--link dk-eviroment-alastria:dk-eviroment-alastria  \
-p 8080:80 \
-p 22001:22001 \
-d img-nginx

## Encendemos/Apagamos el contenedor
docker start dk-nginx


docker stop dk-nginx

## Accedemos al contenedor encedido
docker exec -it dk-nginx bash


# Puertos
Se han habilitado los puertos:

-   **8080**: Para la pagina web. http://localhost:8080/
-   **22001**: Puerto de RPC de conexion con Web3 (Se hara una redirección mediante proxy reverso).
-   **3000**: Puerto por el se puede acceder al acceder al visualizador de estadísticas: [http://localhost:3000/](http://localhost:3000/)
