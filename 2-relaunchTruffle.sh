docker stop dk-truffle
docker rm dk-truffle
docker build -t img-truffle ./dockerTruffle


echo "------------------------------------------------------------------"
echo "## PASO 1 - Desplegar contratos."
echo ">   sudo truffle migrate"
echo "------------------------------------------------------------------"
echo "------------------------------------------------------------------"
echo "## PASO 2 - Generar fichero de configuración"
echo ">   sh getAddress.sh"
echo "------------------------------------------------------------------"
echo "------------------------------------------------------------------"

docker run \
--name dk-truffle \
--link dk-eviroment-alastria:dk-eviroment-alastria  \
-it img-truffle
