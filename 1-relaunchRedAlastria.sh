docker stop dk-eviroment-alastria
docker rm dk-eviroment-alastria
docker build -t img-red-alastria ./dockerRedAlastriaLocal



echo "------------------------------------------------------------------"
echo "------------------------------------------------------------------"
echo "## PASO 1 - Arrancamos la red con 1 validador y 2 regulares."
echo ">   sudo ./bin/start_network.sh clean 1 2"
echo "------------------------------------------------------------------"
echo "------------------------------------------------------------------"
echo "## PASO 2 - Arrancamos el monitor de red."
echo ">   sudo ./bin/start_ethstats.sh"
echo "------------------------------------------------------------------"
echo "------------------------------------------------------------------"
echo "## PASO 3 - Conectamos al nodo general1."
echo ">   sudo geth attach ipc:network/general1/geth.ipc"
echo "------------------------------------------------------------------"
echo "------------------------------------------------------------------"
echo "## PASO 4 - Desbloqueamos la cuenta de admin."
echo ">   web3.personal.unlockAccount(web3.eth.accounts[0], 'Passw0rd',0);"
echo "------------------------------------------------------------------"
echo "------------------------------------------------------------------"


docker run \
--name dk-eviroment-alastria \
-p 3000:3000 \
-it img-red-alastria
