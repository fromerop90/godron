docker stop dk-nginx
docker rm dk-nginx

docker cp  dk-truffle:/home/truffle-project/addressContracts.js ./dockerNginx/web/js/dapp/contracts/

docker build -t img-nginx ./dockerNginx
docker run \
--name dk-nginx \
--link dk-eviroment-alastria:dk-eviroment-alastria  \
-p 8080:80 \
-p 22001:22001 \
-d img-nginx