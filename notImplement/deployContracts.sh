
echo "Ejecutando: sudo truffle migrate ..."
docker exec -d dk-truffle bash -c "sudo truffle migrate"

echo "Ejecutando: sh getAddress.sh ..."
docker exec -it dk-truffle bash -c "sh getAddress.sh"
docker cp  dk-truffle:/home/truffle-project/addressContracts.js ./dockerNginx/web/js/dapp/contracts/
echo "El resultado se ha guardado en ./dockerNginx/web/js/dapp/contracts/addressContracts.js"
echo "(Si el resultado es erroneo, espere y vuelva a ejecutar este script.)"
echo "-----------------------------------------------------------------------------------------------------"
echo "-----------------------------------------------------------------------------------------------------"
echo "----------------------------------RESULTADO ESPERADO-------------------------------------------------"
echo '/*'
echo "Using network 'development'."
echo ""
echo 'Fichero autogenerado por: Francisco Romero Porras con fecha: 9/27/2020, 9:27:12 AM'
echo '*/'
echo 'const dir_airCoin = "0x6d76a44a0Ee642955C9c7C640CC08558D7DfbB1F";'
echo 'const dir_drone = "0xe37a46Cd7d4b977dE3b6e71bDBb9ECA5D8D6d0EA";'
echo 'const dir_plot = "0xC00ae0d8302cafd1544b9d370f194a86176317D4";'
echo 'const dir_goDronService = "0x24AFa3eB5d2a4C57Ee2076db5866Df212Ff2BB62";'
echo 'const presticidesTotalList = ["A", "B", "C", "D", "E"];'
echo 'const admin_address = "0x74d4c56d8dcbc10a567341bfac6da0a8f04dc41d";'
echo "-----------------------------------------------------------------------------------------------------"
echo "-----------------------------------------------------------------------------------------------------"
echo "-----------------------------------------------------------------------------------------------------"
echo "----------------------------------RESULTADO OBTENIDO-------------------------------------------------"
cat ./dockerNginx/web/js/dapp/contracts/addressContracts.js