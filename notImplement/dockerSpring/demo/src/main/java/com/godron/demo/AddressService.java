package com.godron.demo;


import java.net.UnknownHostException;

public interface AddressService {

    String getServerAddress() throws Exception;
}