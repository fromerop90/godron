package com.godron.app.model;

import java.math.BigInteger;
import java.util.List;

public class Drone {
	
	private BigInteger tokenId;
	private Long minFlyHeight;
	private Long maxFlyHeight;
	private List<AvailablePest> availablePesticides;
	private Long cost;
	private BigInteger releaseTime;
	private String owner;
    
	public BigInteger getTokenId() {
		return tokenId;
	}
	public void setTokenId(BigInteger tokenId) {
		this.tokenId = tokenId;
	}
	public Long getMinFlyHeight() {
		return minFlyHeight;
	}
	public void setMinFlyHeight(Long minFlyHeight) {
		this.minFlyHeight = minFlyHeight;
	}
	public Long getMaxFlyHeight() {
		return maxFlyHeight;
	}
	public void setMaxFlyHeight(Long maxFlyHeight) {
		this.maxFlyHeight = maxFlyHeight;
	}
	public List<AvailablePest> getAvailablePesticides() {
		return availablePesticides;
	}
	public void setAvailablePesticides(List<AvailablePest> availablePesticides) {
		this.availablePesticides = availablePesticides;
	}
	public Long getCost() {
		return cost;
	}
	public void setCost(Long cost) {
		this.cost = cost;
	}
	public BigInteger getReleaseTime() {
		return releaseTime;
	}
	public void setReleaseTime(BigInteger releaseTime) {
		this.releaseTime = releaseTime;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
    
}
