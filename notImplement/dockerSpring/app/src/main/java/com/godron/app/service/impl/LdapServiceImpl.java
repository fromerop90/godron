//package com.godron.app.service.impl;
//
//import java.util.List;
//import java.util.stream.Collectors;
//
//import org.fmbbva.movcor.backoffice.ad.domain.PersonAD;
//import org.fmbbva.movcor.backoffice.ad.domain.PersonADPro;
//import org.fmbbva.movcor.backoffice.ad.service.ADPersonService;
//import org.fmbbva.movcor.backoffice.db.domain.User;
//import org.fmbbva.movcor.backoffice.exceptions.LdapServiceException;
//import org.fmbbva.movcor.backoffice.ldap.domain.Person;
//import org.fmbbva.movcor.backoffice.ldap.service.PersonService;
//import org.fmbbva.movcor.backoffice.services.LdapService;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.stereotype.Service;
//
///**
// * @author xe35370
// * Implementación de servicios de ldap
// */
//@Service
//public class LdapServiceImpl implements LdapService {
//	
//	private static Logger logger = LoggerFactory.getLogger(LdapServiceImpl.class);
//	
//	private PersonService personService;
//	
//	private ADPersonService aDPersonService;
//	
//	@Value("${movcli.dualAdLdapBasePro}")
//	private boolean isPro;
//	
//	/**
//	 * Constructor
//	 * @param personService
//	 * @param aDPersonService
//	 */
//	public LdapServiceImpl(@Autowired PersonService personService,
//			@Autowired ADPersonService aDPersonService) {
//		super();
//		this.personService = personService;
//		this.aDPersonService = aDPersonService;
//	}
//	
//	/* (non-Javadoc)
//	 * @see org.fmbbva.movcor.app.services.LdapService#blockClient(java.lang.String)
//	 */
//	@Override
//	public void blockClient(String id) throws LdapServiceException {
//		logger.debug("blockCliente");
//	}
//	
//	/* (non-Javadoc)
//	 * @see org.fmbbva.movcor.backoffice.services.LdapService#addUser(org.fmbbva.movcor.backoffice.db.domain.User, java.lang.String)
//	 */
//	@Override
//	public void addUser(User user, String password) throws LdapServiceException {
//		try{
//			logger.debug("addClient");
//			personService.createUsuario(getPersonFromUser(user, password));
//		}catch(Exception e){
//			logger.error(e.getMessage());	
//			throw new LdapServiceException("Error insertando usuario en LDAP con el error:  " +e.getCause().getMessage() , e);
//		}
//	}
//	
//	/* (non-Javadoc)
//	 * @see org.fmbbva.movcor.backoffice.services.LdapService#findUser(org.fmbbva.movcor.backoffice.db.domain.User)
//	 */
//	@Override
//	public List<User> findUser(User user) throws LdapServiceException {
//		try{
//			logger.debug("findUser");
//			return getUsersFromPeople(personService.findPeople(user.getId(), user.getName(), user.getLastName()));
//		}catch(Exception e){
//			logger.error(e.getMessage());
//			throw new LdapServiceException("Error buscando usuario en LDAP", e);
//		}
//	}
//	
//	/* (non-Javadoc)
//	 * @see org.fmbbva.movcor.backoffice.services.LdapService#findADUser(org.fmbbva.movcor.backoffice.db.domain.User)
//	 */
//	@Override
//	public List<User> findADUser(User user) throws LdapServiceException {
//		try{
//			logger.debug("findADUser");
//			if(isPro) {
//				return getUsersFromADProPeople(aDPersonService.findPeoplePro(user.getId(), user.getName(), user.getLastName()));
//			}else {
//				return getUsersFromADPeople(aDPersonService.findPeople(user.getId(), user.getName(), user.getLastName()));
//			}
//			
//		}catch(Exception e){
//			logger.error(e.getMessage());
//			throw new LdapServiceException("Error buscando usuario en LDAP", e);
//		}
//	}
//	
//	/* (non-Javadoc)
//	 * @see org.fmbbva.movcor.backoffice.services.LdapService#deleteUser(org.fmbbva.movcor.backoffice.db.domain.User)
//	 */
//	@Override
//	public void deleteUser(User user) throws LdapServiceException {
//		try{
//			logger.debug("deleteUser");
//			personService.deleteUsuario(getPersonFromUser(user, ""));
//		}catch(Exception e){
//			logger.error(e.getMessage());
//			throw new LdapServiceException("Error eliminando usuario de LDAP", e);
//		}
//	}
//	
//	/**
//	 * Crea un usuario para el LDAP con datos básicos
//	 * @param user
//	 * @param password
//	 * @return
//	 */
//	private Person getPersonFromUser(User user, String password){
//		return Person.builder()
//				.id(new StringBuilder().append("uid=").append(user.getId()).append(",ou=people").toString()).uid(user.getId())
//				.firstName(user.getName()).lastName(user.getName()).fullName(user.getName())
//				.password(password).movil(user.getPhone()).build();
//	}
//	
//	/**
//	 * @param people
//	 * @return
//	 */
//	private List<User> getUsersFromPeople(List<Person> people){
//		return people.stream().map(p -> getUserFromPerson(p)).collect(Collectors.toList());
//	}
//	
//	/**
//	 * @param people
//	 * @return
//	 */
//	private List<User> getUsersFromADPeople(List<PersonAD> peopleAD){
//		return peopleAD.stream().map(p -> getUserFromPersonAD(p)).collect(Collectors.toList());
//	}
//
//	/**
//	 * @param people
//	 * @return
//	 */
//	private List<User> getUsersFromADProPeople(List<PersonADPro> peopleADPro){
//		return peopleADPro.stream().map(p -> getUserFromPersonADPro(p)).collect(Collectors.toList());
//	}
//	
//	/**
//	 * Crea un usuario para el LDAP con datos básicos
//	 * @param person
//	 * @param password
//	 * @return
//	 */
//	private User getUserFromPerson(Person person){
//		return User.builder()
//				.id(person.getUid())
//				.name(person.getFirstName())
//				.lastName(person.getLastName())
//				.build();
//	}
//	
//	/**
//	 * Crea un usuario para el LDAP con datos básicos
//	 * @param person
//	 * @param password
//	 * @return
//	 */
//	private User getUserFromPersonAD(PersonAD person){
//		return User.builder()
//				.id(person.getUid())
//				.name(person.getFirstName())
//				.lastName(person.getLastName())
//				.build();
//	}
//	
//	/**
//	 * Crea un usuario para el LDAP con datos básicos
//	 * @param person
//	 * @param password
//	 * @return
//	 */
//	private User getUserFromPersonADPro(PersonADPro person){
//		return User.builder()
//				.id(person.getUid())
//				.name(person.getFirstName())
//				.lastName(person.getLastName())
//				.build();
//	}
//
//}
