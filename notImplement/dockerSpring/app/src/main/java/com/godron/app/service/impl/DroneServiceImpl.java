package com.godron.app.service.impl;

import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.Request;
import org.web3j.protocol.core.methods.response.EthAccounts;
import org.web3j.protocol.core.methods.response.Web3ClientVersion;
import org.web3j.protocol.http.HttpService;

import com.godron.app.service.DroneService;

/**
 * @author xe35370
 * Implementación de servicios de ldap
 */
@Service
public class DroneServiceImpl implements DroneService {
	
	private static Logger logger = LoggerFactory.getLogger(DroneServiceImpl.class);
	
//	@Value("${movcli.dualAdLdapBasePro}")
//	private boolean isPro;

	@Override
	public List<String> getAccountList(String droneTokenId, String userSender) {
		logger.info("getAccountList() - Empieza con: {}, {}", droneTokenId, userSender);
		Web3j web3 = Web3j.build(new HttpService("http://dk-eviroment-alastria:22001"));  // defaults to http://localhost:8545/
		List<String> accountList=null;;
		try {
			accountList = web3.ethAccounts().send().getAccounts();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		logger.info("getAccountList() - Termina con: {}", accountList);
		return accountList;
	}
	


}
