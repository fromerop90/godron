package com.godron.app.model;

public class AvailablePest {

	private String presticideCode;
	private Boolean available;
	
	public String getPresticideCode() {
		return presticideCode;
	}
	public void setPresticideCode(String presticideCode) {
		this.presticideCode = presticideCode;
	}
	public Boolean getAvailable() {
		return available;
	}
	public void setAvailable(Boolean available) {
		this.available = available;
	}
}
