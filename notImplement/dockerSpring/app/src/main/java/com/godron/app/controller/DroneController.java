package com.godron.app.controller;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.godron.app.model.Drone;
import com.godron.app.service.DroneService;

@Controller
public class DroneController {

	private static Logger logger = LoggerFactory.getLogger(DroneController.class);
	
	@Autowired
	private DroneService droneService;
	
	@GetMapping("/droneList")
    public String droneList(Model model) {
		logger.info("droneList() - Empieza");
		
		String username = "fromero";
		model.addAttribute("username", username);
		model.addAttribute("listaDrones", mockListaDrones());
		
		logger.info("droneList() - Termina");
        return "droneHome";
    }
	
	@PostMapping("/droneAdd")
    public String droneAdd(Drone body, Model model) {
		logger.info("droneAdd() - Empieza con: {}", body);
		List<Drone> listaDrones = mockListaDrones();
		listaDrones.add(body);
		model.addAttribute("listaDrones", listaDrones);
		logger.info("droneAdd() - Termina");
        return "droneHome";
    }
	
	@GetMapping("/accountList")
    public String accountList(String droneTokenId, Model model) {
		logger.info("accountList() - Empieza con: {}", droneTokenId);
		String userSender = "0x01";
		List<String> accountList = droneService.getAccountList(droneTokenId, userSender);
		model.addAttribute("accountList", accountList);
		logger.info("accountList() - Termina");
        return "accountList";
    }
	
	private List<Drone> mockListaDrones(){
		List<Drone> listaDrones = new ArrayList<>();
		Drone drone1 = new Drone();
		drone1.setTokenId(BigInteger.ONE);
		drone1.setOwner("0x1");
		listaDrones.add(drone1);
		
		Drone drone2 = new Drone();
		drone2.setTokenId(BigInteger.valueOf(2L));
		drone2.setOwner("0x2");
		listaDrones.add(drone2);
		return listaDrones;
	}
}
