//package com.godron.app.service;
//
//import java.util.List;
//
//import org.fmbbva.movcor.backoffice.db.domain.User;
//import org.fmbbva.movcor.backoffice.exceptions.LdapServiceException;
//
///**
// * @author XE35370 Interfaz para el uso de LDAP
// */
//public interface LdapService {
//	
//	/**
//	 * Se bloquea el cliente en el LDAP
//	 */
//	void blockClient(String id) throws LdapServiceException;
//	
//	/**
//	 * Se adiciona un usuario de corresponsal en LDAP
//	 * @param user
//	 * @param password
//	 * @throws LdapServiceException
//	 */
//	void addUser(User user, String password) throws LdapServiceException;
//	
//	/**
//	 * Se realiza una búsqueda de usuarios en LDAP
//	 * @param user
//	 * @return
//	 * @throws LdapServiceException
//	 */
//	List<User> findUser(User user) throws LdapServiceException;
//	
//	/**
//	 * Se realiza una búsqueda de usuarios en AD
//	 * @param user
//	 * @return
//	 * @throws LdapServiceException
//	 */
//	List<User> findADUser(User user) throws LdapServiceException;
//	
//	/**
//	 * Se elimina un usuario de corresponsal del LDAP
//	 * @param user
//	 * @throws LdapServiceException
//	 */
//	void deleteUser(User user) throws LdapServiceException;
//
//}
