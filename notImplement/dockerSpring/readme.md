# Red Local de Alastria en Docker

Microservicio de Spring Boot en imagen docker ([spring.io](https://spring.io/guides/gs/spring-boot-docker/))
# Files
Dockerfile
## Personalizar
Se podrá personalizar el el contenido de la web en la carpeta 

## Instalar Docker
Seguir la documentación oficial para instalar Docker [https://docs.docker.com/get-docker/](https://docs.docker.com/get-docker/)

## Generar imagen personalizada a partir del Dockerfile.
docker build -t img-spring-demo ./dockerSpring


## Iniciarmos el contendor
docker run \
--name dk-spring-demo \
-p 8011:8011 \
-d img-spring-demo


## Encendemos/Apagamos el contenedor
docker start dk-spring-demo


docker stop dk-spring-demo

## Accedemos al contenedor encedido
docker exec -it dk-spring-demo bash


# Puertos
Se han habilitado los puertos:

-   **8011**: Para la exponer por REST el microservicio 
 curl --location --request GET 'http://localhost:8011/micro-service'
