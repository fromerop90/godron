echo "------------------------------------------------------------------------------------"
echo "Agregamos el nuevo usuario a Ldap"
echo "AddUser Empieza"
ldapadd -x -D "cn=admin,dc=godron,dc=com" -w $LDAP_ADMIN_PASSWORD -f $NEW_USER_FILE_PATH
echo "AddUser Termina con el siguiente listado de usuarios:"
echo "------------------------------------------------------------------------------------"
ldapsearch -x -H ldap://localhost -b dc=godron,dc=com -D "cn=readonly,dc=godron,dc=com" -w $LDAP_READONLY_USER_PASSWORD