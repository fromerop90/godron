# Red Local de Alastria en Docker


# Files
Dockerfile
## Personalizar
Pondremos los datos iniciales en ./bootstrap.ldif

## Instalar Docker
Seguir la documentación oficial para instalar Docker [https://docs.docker.com/get-docker/](https://docs.docker.com/get-docker/)

## Generar imagen personalizada a partir del Dockerfile.
docker build -t img-openldap ./dockerOpenLdap


## Iniciarmos el contendor

docker run \
--name dk-openldap \
-p 389:389 \
-p 8090:8090 \
-d img-openldap 

## Encendemos/Apagamos el contenedor
docker start dk-openldap


docker stop dk-openldap

## Accedemos al contenedor encedido
docker exec -it dk-openldap bash

## Añadimos el usuario inicial
docker exec -it dk-openldap bash -c "sh addUser.sh"

## Consultamos en Ldap Admin
docker exec dk-openldap ldapsearch -x -H ldap://localhost -b dc=godron,dc=com -D "cn=admin,dc=godron,dc=com" -w $LDAP_ADMIN_PASSWORD

## Consultamos en Ldap Readonly
docker exec dk-openldap ldapsearch -x -H ldap://localhost -b dc=godron,dc=com -D "cn=readonly,dc=godron,dc=com" -w $LDAP_READONLY_USER_PASSWORD

# Puertos
Se han habilitado los puertos:

-   **3306**: Después de implementar los servicios, el servidor LDAP estará disponible en la siguiente URL: http://127.0.0.1:389.
-   **8090**: Además, podrá navegar por el servidor LDAP, ver sus recursos y crear nuevos conectándose a la siguiente URL http://127.0.0.1:8090.

