docker stop dk-openldap
docker rm dk-openldap
docker build -t img-openldap ./dockerOpenLdap
docker run \
--name dk-openldap \
-p 389:389 \
-p 8090:8090 \
-d img-openldap 
