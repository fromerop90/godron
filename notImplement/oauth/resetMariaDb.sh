docker stop dk-mariadb-oauth
docker rm dk-mariadb-oauth
docker build -t img-mariadb-oauth ./dockerMariaDBOauth
docker run \
--name dk-mariadb-oauth \
-p 3306:3306 \
-d img-mariadb-oauth 
docker exec -it dk-mariadb-oauth bash -c "sh init-database.sh"