# Red Local de Alastria en Docker


# Files
Dockerfile
## Personalizar
Pondremos los script de creación de la BBDD en /sql-scripts/initial-data.sql

## Instalar Docker
Seguir la documentación oficial para instalar Docker [https://docs.docker.com/get-docker/](https://docs.docker.com/get-docker/)

## Generar imagen personalizada a partir del Dockerfile.
docker build -t img-mariadb-oauth ./dockerMariaDBOauth


## Iniciarmos el contendor

docker run \
--name dk-mariadb-oauth \
-p 3306:3306 \
-d img-mariadb-oauth 

## Encendemos/Apagamos el contenedor
docker start dk-mariadb-oauth


docker stop dk-mariadb-oauth

## Inicializamos la BBDD
docker exec -it dk-mariadb-oauth bash -c "sh init-database.sh"


## Accedemos al contenedor encedido
docker exec -it dk-mariadb-oauth bash

##  Comandos mysql

# Conectar a bbdd
mysql -u root -p$MYSQL_ROOT_PASSWORD $MYSQL_DATABASE

# Muestra los esquemas
show databases;

# Muestra las tablas
show tables;

# Crea un esquema
create database mariaDBdocker;

# Utiliza un esquema
use mariaDBdocker;


# Puertos
Se han habilitado los puertos:

-   **3306**: Para la conexión con MySQL


