echo "------------------------------------------------------------------------------------"
echo "Esperando 1 min a que arranque la BBDD para cargar los SQL..."
sleep 1m
echo "La Carga SQL Empieza"
mysql -u root -p$MYSQL_ROOT_PASSWORD $MYSQL_DATABASE < /home/sql-scripts/initial-data.sql
echo "La Carga SQL Termina"