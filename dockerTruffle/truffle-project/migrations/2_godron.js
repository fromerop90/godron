var AirCoin = artifacts.require("./AirCoin.sol");
var Drone = artifacts.require("./Drone.sol");
var Plot = artifacts.require("./Plot.sol");
var GoDronService = artifacts.require("./GoDronService.sol");

module.exports = function(deployer) {
    deployer.deploy(AirCoin, 0)
		//Desplegamos AirCoin
        .then(() => AirCoin.deployed())
        .then(() => 
				//Desplegamos Drone
				deployer.deploy(Drone)
					.then(() => Drone.deployed())
					.then(() => 
							//Desplegamos Plot
							deployer.deploy(Plot)
								.then(() => Plot.deployed())
								//Desplegamos GoDronService
								.then(() =>  deployer.deploy(GoDronService, AirCoin.address, Drone.address, Plot.address))
							)
				);
}