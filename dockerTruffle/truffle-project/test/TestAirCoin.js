const AirCoin = artifacts.require("AirCoin");
const truffleAssert = require("truffle-assertions");

contract("AirCoin", async accounts => {
    let airCoinInstance;

    beforeEach(async () => {
        airCoinInstance = await AirCoin.new(1000);
    });

    afterEach(async () => {
    });

    it("AirCoin - totalSupply 1000", async () => {
        let totalSupply = await airCoinInstance.totalSupply();
		assert.equal(totalSupply, 1000, "Invalid totalSupply: " + totalSupply);
    });

    it("AirCoin - createNewToken", async () => {
		airCoinInstance.createNewToken(1000).on('receipt', function(receipt) {
			let isEvento = receipt.events.createNewTokenAirCoin != undefined;
			assert.isTrue(isEvento,"Error createNewToken")
			airCoinInstance.totalSupply().call(function(error, result) {
				assert.isTrue(!error,"Error createNewToken")
				assert.equal(result, 2000, "Invalid totalSupply: " + result);
			});
		});
    });
});
