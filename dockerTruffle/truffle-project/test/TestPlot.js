const Plot = artifacts.require("Plot");
const truffleAssert = require("truffle-assertions");

contract("Plot", async accounts => {
    let plotInstance;
    let lastPlotId;

    beforeEach(async () => {
        plotInstance = await Plot.new();
    });

    afterEach(async () => {
    });

    it("Plot - totalSupply 0", async () => {
        let totalSupply = await plotInstance.totalSupply();
		assert.equal(totalSupply, 0, "Invalid totalSupply: " + totalSupply);
    });

    it("Plot - createNewToken", async () => {
		plotInstance.createNewToken(accounts[0], 1,2,"A").on('receipt', function(receipt) {
			let isEvento = receipt.events.updateTokenPlot != undefined;
			assert.isTrue(isEvento,"Error createNewToken")
			plotInstance.totalSupply().call(function(error, result) {
				assert.isTrue(!error,"Error createNewToken")
				assert.equal(result, 1, "Invalid totalSupply: " + result);
				lastPlotId = result;
			});
		});
    });
	
    it("Plot - set/getMinFlyHeight", async () => {
		airCoinInstance.setMinFlyHeight(lastPlotId, 11).on('receipt', function(receipt) {
			let isEvento = receipt.events.updateTokenPlot != undefined;
			assert.isTrue(isEvento,"Error createNewToken")
			airCoinInstance.getMinFlyHeight(lastPlotId).call(function(error, result) {
				assert.isTrue(!error,"Error createNewToken")
				assert.equal(result, 11, "Invalid MinFlyHeight: " + result);
			});
		});
    });
	
    it("Plot - set/getMaxFlyHeight", async () => {
		airCoinInstance.setMaxFlyHeight(lastPlotId, 2).on('receipt', function(receipt) {
			let isEvento = receipt.events.updateTokenPlot != undefined;
			assert.isTrue(isEvento,"Error createNewToken")
			airCoinInstance.getMaxFlyHeight(lastPlotId).call(function(error, result) {
				assert.isTrue(!error,"Error createNewToken")
				assert.equal(result, 22, "Invalid MaxFlyHeight: " + result);
			});
		});
    });
	
    it("Plot - set/getAvailablePesticide", async () => {
		airCoinInstance.setAvailablePesticide(lastPlotId, "A").on('receipt', function(receipt) {
			let isEvento = receipt.events.updateTokenPlot != undefined;
			assert.isTrue(isEvento,"Error createNewToken")
			airCoinInstance.getAvailablePesticide(lastPlotId).call(function(error, result) {
				assert.isTrue(!error,"Error createNewToken")
				assert.equal(result, "B", "Invalid AvailablePesticide: " + result);
			});
		});
    });
	
    it("Plot - set/getOwnerPlotAddress", async () => {
		airCoinInstance.setOwnerPlotAddress(lastPlotId, accounts[0]).on('receipt', function(receipt) {
			let isEvento = receipt.events.updateTokenPlot != undefined;
			assert.isTrue(isEvento,"Error createNewToken")
			airCoinInstance.getOwnerPlotAddress(lastPlotId).call(function(error, result) {
				assert.isTrue(!error,"Error createNewToken")
				assert.equal(result, accounts[0], "Invalid getOwnerPlotAddress: " + result);
			});
		});
    });
});
