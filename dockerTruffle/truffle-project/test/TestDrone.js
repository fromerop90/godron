const Drone = artifacts.require("Drone");
const truffleAssert = require("truffle-assertions");

contract("Drone", async accounts => {
    let droneInstance;
    let lastTokenId;

    beforeEach(async () => {
        droneInstance = await Drone.new();
    });

    afterEach(async () => {
    });

    it("Drone - totalSupply 0", async () => {
        let totalSupply = await droneInstance.totalSupply();
		assert.equal(totalSupply, 0, "Invalid totalSupply: " + totalSupply);
    });

    it("Drone - createNewToken", async () => {
		droneInstance.createNewToken(1,2,2).on('receipt', function(receipt) {
			let isEvento = receipt.events.createNewTokenDrone != undefined;
			assert.isTrue(isEvento,"Error createNewToken")
			droneInstance.totalSupply().call(function(error, result) {
				assert.isTrue(!error,"Error createNewToken")
				assert.equal(result, 1, "Invalid totalSupply: " + result);
				lastTokenId = result;
			});
		});
    });
	
    it("Drone - set/getMinFlyHeight", async () => {
		airCoinInstance.setMinFlyHeight(lastTokenId, 11).on('receipt', function(receipt) {
			let isEvento = receipt.events.updateTokenPlot != undefined;
			assert.isTrue(isEvento,"Error createNewToken")
			airCoinInstance.getMinFlyHeight(lastTokenId).call(function(error, result) {
				assert.isTrue(!error,"Error createNewToken")
				assert.equal(result, 11, "Invalid MinFlyHeight: " + result);
			});
		});
    });
	
    it("Drone - set/getMaxFlyHeight", async () => {
		airCoinInstance.setMaxFlyHeight(lastTokenId, 33).on('receipt', function(receipt) {
			let isEvento = receipt.events.updateTokenPlot != undefined;
			assert.isTrue(isEvento,"Error createNewToken")
			airCoinInstance.getMaxFlyHeight(lastTokenId).call(function(error, result) {
				assert.isTrue(!error,"Error createNewToken")
				assert.equal(result, 33, "Invalid MaxFlyHeight: " + result);
			});
		});
    });
	
    it("Drone - set/getCost", async () => {
		airCoinInstance.setCost(lastTokenId, 44).on('receipt', function(receipt) {
			let isEvento = receipt.events.updateTokenPlot != undefined;
			assert.isTrue(isEvento,"Error createNewToken")
			airCoinInstance.getCost(lastTokenId).call(function(error, result) {
				assert.isTrue(!error,"Error createNewToken")
				assert.equal(result, 44, "Invalid Cost: " + result);
			});
		});
    });
	
    it("Drone - addPesticide/isAvailablePesticide", async () => {
		airCoinInstance.addPesticide(lastTokenId, "A", true).on('receipt', function(receipt) {
			let isEvento = receipt.events.updateTokenPlot != undefined;
			assert.isTrue(isEvento,"A")
			airCoinInstance.isAvailablePesticide(lastTokenId).call(function(error, result) {
				assert.isTrue(!error,"Error createNewToken")
				assert.equal(result, true, "Invalid getOwnerPlotAddress: " + result);
			});
		});
    });
});
