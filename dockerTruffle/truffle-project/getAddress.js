const config = require("/home/truffle-project/truffle-config.js");

var AirCoin = artifacts.require("./AirCoin.sol");
var Drone = artifacts.require("./Drone.sol");
var Plot = artifacts.require("./Plot.sol");
var GoDronService = artifacts.require("./GoDronService.sol");

module.exports = function(deployer, network) {
	AirCoin.deployed();
	Drone.deployed();
	Plot.deployed();
	GoDronService.deployed();
	var currentDate = new Date().toLocaleString();
	console.log("Fichero autogenerado por: Francisco Romero Porras con fecha: " + currentDate)
	console.log("*/");
	console.log('const dir_airCoin = "'+ AirCoin.address +'";');
	console.log('const dir_drone = "'+ Drone.address +'";');
	console.log('const dir_plot = "'+ Plot.address +'";');
	console.log('const dir_goDronService = "'+ GoDronService.address +'";');
	console.log('const presticidesTotalList = ["A", "B", "C", "D", "E"];');
	console.log('const admin_address = "'+ config.networks.development.from +'";');
}
