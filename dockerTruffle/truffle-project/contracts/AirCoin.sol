// contracts/AirCoin.sol
// SPDX-License-Identifier: fromerop90@gmail.com
pragma solidity ^0.4.24;

import "./openzeppelin/token/ERC20/ERC20.sol";
import "./openzeppelin/token/ERC20/ERC20Detailed.sol";
import "./openzeppelin/ownership/Ownable.sol";

/**
 * @author fromerop90@gmail.com
 * name: este m�todo devuelve el nombre del token dentro de la red de Ethereum.
 * symbol: este m�todo devuelve el s�mbolo del token asignado para su f�cil reconocimiento.
 * decimals: este m�todo devuelve n�mero de decimales en los que se puede dividir el token, 
 * 			con fines de simplicidad de almacenamiento de la informaci�n. En el contrato 
 * 			siempre se almacena el token en su unidad m�nima; utilizando la informaci�n 
 * 			de los decimales se puede recuperar la informaci�n en su unidad est�ndar.
 * totalSupply: este m�todo devuelve el n�mero total de tokens en circulaci�n.
 * balanceOf: este m�todo devuelve la cantidad de tokens en posesi�n de la direcci�n 
 * 			introducida, siempre en la unidad m�nima.
 * transfer: este m�todo transfiere una cierta cantidad de tokens, en posesi�n del 
 * 			emisor del mensaje, al destinatario especificado.
 * transferFrom: este m�todo permite transferir tokens de una cierta cuenta a otra, 
 * 			siempre que la transferencia est� aprobada.
 * approve: este m�todo permite aprobar el gasto de tokens para que un tercero haga 
 * 			el movimiento de tokens cuando sea necesario.
 * transfer: evento emitido durante una transferencia con la informaci�n de la misma.
 * aproval: evento emitido durante la aprobaci�n de gasto por parte de un tercero.
 */
contract AirCoin is ERC20, ERC20Detailed, Ownable {
    constructor(uint256 initialSupply) ERC20Detailed("AirCoin", "AIR", 0) public {
        _mint(msg.sender, initialSupply);
    }
    
    event createNewTokenAirCoin (uint256 amount);
    function createNewToken(uint256 amount) public onlyOwner{
        _mint(owner(), amount);
        emit createNewTokenAirCoin(amount);
    }
    
}
