// contracts/GoDronService.sol
// SPDX-License-Identifier: fromerop90@gmail.com
pragma solidity ^0.4.24;

import "./AirCoin.sol";
import "./Drone.sol";
import "./Plot.sol";

contract GoDronService is Ownable {
    AirCoin private _airCoinContract;
    Drone private _droneContract;
    Plot private _plotContract;
    bool flagEmitEvent = false;
    
    constructor(address _airCoinContractAddress, 
        address _droneContractAddress, address _plotContractAddress) public {
        _airCoinContract = AirCoin(_airCoinContractAddress);
        _droneContract = Drone(_droneContractAddress);
        _plotContract = Plot(_plotContractAddress);
    }

    function droneOwnerOf(uint256 tokenId) public view returns (address) {
        return _droneContract.ownerOf(tokenId);
    }

    function plotOwnerOf(uint256 tokenId) public view returns (address) {
        return _plotContract.ownerOf(tokenId);
    }

    modifier sameTokenOwner(uint256 droneTokenId, uint256 plotTokenId) {
        require(_plotContract.ownerOf(plotTokenId)==_droneContract.ownerOf(droneTokenId) ,
            "ERROR: The drone and the plot do not belong to the same company." );
        _;
    }
    
    event PlotFumigated(uint256 droneTokenId, uint256 plotTokenId);
    function fumigate(uint256 droneTokenId, uint256 plotTokenId) public  sameTokenOwner(droneTokenId, plotTokenId) returns (uint256){
        require(_droneContract.getMinFlyHeight(droneTokenId)>=_plotContract.getMinFlyHeight(plotTokenId) 
            ,"ERROR: The minimum flight height of the Drone must be greater than or equal to that of the Plot.");
        require(_droneContract.getMaxFlyHeight(droneTokenId)<=_plotContract.getMinFlyHeight(plotTokenId) 
            ,"ERROR: The maximum flight height of the Drone must be less than or equal to that of the Plot."); 
        require(_droneContract.isAvailablePesticide(droneTokenId, _plotContract.getAvailablePesticide(plotTokenId))
            ,"ERROR: The selected Drone does not have the correct pesticide for the Plot.");
        if(_droneContract.isOnTime(droneTokenId)){
            //Devolvemos la propiedad del Dron al administrador.
            _droneContract.setManagementCompanyAddress(droneTokenId, owner());
            emit PlotFumigated(droneTokenId, plotTokenId);
            return plotTokenId;
        }else{
            _droneContract.release(droneTokenId);
        }
    }

    /**
     * En caso exitoso se emitiran los siguientes eventos:
     * ERC20 - Transfer(managementCompanyAddress, owner, rentCost);
     * ERC721 - Transfer(owner, managementCompanyAddress, droneTokenId);
     * DroneContract - UpdateTokenDrone(droneTokenId)
     * GoDronService - DroneRented(droneTokenId, managementCompanyAddress);
     */
    event DroneRented(uint256 droneTokenId, address managementCompanyAddress);
    function rentDrone(uint256 droneTokenId, address managementCompanyAddress) public onlyOwner returns (uint256){
        if(flagEmitEvent==true){
            flagEmitEvent = false;
        }else{
            flagEmitEvent = true;
        }
        //Comprobamos que el token del dron este en propiedad de GoDron
        require(owner()==_droneContract.ownerOf(droneTokenId) ,"ERROR: This drone is already rented by another company." );
        //Obtenemos el coste del alquiler
        uint256 rentCost = _droneContract.getCost(droneTokenId);
        //Obtenemos el saldo del cliente
        uint256 balanceOf = _airCoinContract.balanceOf(managementCompanyAddress);
        //Comprobamos la cuenta del cliente tenga saldo para pagar el alquiler.
        require(balanceOf >= rentCost ,"ERROR: The company does not have enough balance to pay the cost of the drone rental." );
        //Cobramos el coste del alquiler
        _airCoinContract.transferFrom(managementCompanyAddress, owner(), rentCost);
        //Transferimos la propiedad del Dron a la Compañia.
        _droneContract.setManagementCompanyAddress(droneTokenId, managementCompanyAddress);
        //Asignamos 86400 segundos que son 24H
        _droneContract.setReleaseTime(droneTokenId, 86400);
        emit DroneRented(droneTokenId, managementCompanyAddress);
        return droneTokenId;
    }
}