// contracts/Drone.sol
// SPDX-License-Identifier: fromerop90@gmail.com
pragma solidity ^0.4.24;

import "./openzeppelin/token/ERC721/ERC721Full.sol";
import "./openzeppelin/ownership/Ownable.sol";
import "./openzeppelin/drafts/Counter.sol";

contract Drone is ERC721Full, Ownable {
    using Counter for Counter.Counter;
    Counter.Counter private _autoNumId;
    
    // Atributos
    struct Detail{
        uint256 _minFlyHeight;
        uint256 _maxFlyHeight;
        mapping (string => bool) _availablePesticides;
        uint256 _cost;
        // timestamp when token release is enabled (seconds)
        uint256 _releaseTime;
    }

    mapping (uint256 => Detail) public mapDetails;

    constructor() public ERC721Full("GoDrone", "GD") {
        _autoNumId.current=1;
    }
    
    modifier validParams(uint256 minFlyHeight, 
            uint256 maxFlyHeight, uint256 cost) {
        require(minFlyHeight<=maxFlyHeight,"ERROR: minFlyHeight must be less than or equal to the maxFlyHeight." );
        require(cost>0,"ERROR: cost is zero." );
        _;
    }

    event createNewTokenDrone (uint256 tokenId);
    function createNewToken(uint256 minFlyHeight, uint256 maxFlyHeight, uint256 cost)
            public onlyOwner 
            validParams(minFlyHeight, maxFlyHeight, cost)
            returns (uint256) {
                
        uint256 tokenId = _autoNumId.current;
        _mint(owner(), tokenId);
        _setTokenURI(tokenId, "");
        
        mapDetails[tokenId]._minFlyHeight = minFlyHeight;
        mapDetails[tokenId]._maxFlyHeight = maxFlyHeight;
        mapDetails[tokenId]._cost = cost;
        mapDetails[tokenId]._releaseTime = block.timestamp;
		
		_autoNumId.next();
        emit createNewTokenDrone(tokenId);
        return tokenId;
    }
    
    function destroyToken(uint256 tokenId) public onlyOwner{
        _burn(msg.sender, tokenId);
    }

    // Getters
    function getMinFlyHeight(uint _tokenId) public view returns(uint256)  {
        return mapDetails[_tokenId]._minFlyHeight;
    }
    function getMaxFlyHeight(uint _tokenId) public view returns(uint256)  {
        return mapDetails[_tokenId]._maxFlyHeight;
    }
    function getCost(uint _tokenId) public view returns(uint256)  {
        return mapDetails[_tokenId]._cost;
    }
    function isAvailablePesticide(uint _tokenId, string _pest) public view returns(bool)  {
        return mapDetails[_tokenId]._availablePesticides[_pest];
    }
    function getManagementCompanyAddress(uint _tokenId) public view returns(address)  {
        return ownerOf(_tokenId);
    }
    function getReleaseTime(uint _tokenId) public view returns(uint256)  {
        return mapDetails[_tokenId]._releaseTime;
    }
    
    event ExpiredDrone(uint256 droneTokenId);
    function isOnTime(uint _tokenId) public returns(bool)  {
        if(mapDetails[_tokenId]._releaseTime<block.timestamp){
            return true;
        }else{
            emit ExpiredDrone(_tokenId);
            return false;
        }
    }

    // Setters
    event UpdateTokenDrone (uint256 tokenId);
    function setMinFlyHeight(uint _tokenId, uint256 _minFlyHeight) public onlyOwner{
        require(_minFlyHeight<=mapDetails[_tokenId]._maxFlyHeight,"ERROR: minFlyHeight must be less than or equal to the maxFlyHeight." );
        mapDetails[_tokenId]._minFlyHeight = _minFlyHeight;
        emit UpdateTokenDrone(_tokenId);
    }
    function setMaxFlyHeight(uint _tokenId, uint256 _maxFlyHeight) public onlyOwner{
        require(mapDetails[_tokenId]._minFlyHeight<=_maxFlyHeight,"ERROR: minFlyHeight must be less than or equal to the maxFlyHeight." );
        mapDetails[_tokenId]._maxFlyHeight = _maxFlyHeight;
        emit UpdateTokenDrone(_tokenId);
    }
    function setCost(uint _tokenId, uint256 _cost) public onlyOwner{
        mapDetails[_tokenId]._cost = _cost;
        emit UpdateTokenDrone(_tokenId);
    }
    
    function compareStrings(string a, string b) internal pure returns (bool) {
        return keccak256(bytes(a)) == keccak256(bytes(b));
    }
    
    modifier validPesticide(string pesticide) {
        require(
            compareStrings(pesticide,"A") || compareStrings(pesticide,"B") || 
            compareStrings(pesticide,"C") || compareStrings(pesticide,"D") || 
            compareStrings(pesticide,"E") 
        ,"ERROR: the pesticide is not valid, the valid pesticides are: A, B, C, D, E" );
        _;
    }
    function addPesticide(uint _tokenId, string _pesticide, bool _isAvailable) public onlyOwner 
            validPesticide(_pesticide){
                
        mapDetails[_tokenId]._availablePesticides[_pesticide] = _isAvailable;
        emit UpdateTokenDrone(_tokenId);
    }
    
    function setManagementCompanyAddress(uint _tokenId, address _managementCompanyAddress) public {
        transferFrom(ownerOf(_tokenId), _managementCompanyAddress, _tokenId);
        //Emitira un evento de ERC721 Transfer(from, to, tokenId);
    }
    function setReleaseTime(uint _tokenId, uint256 _seconds) public {
        mapDetails[_tokenId]._releaseTime = block.timestamp + _seconds;
        emit UpdateTokenDrone(_tokenId);
    }
    function addSecondsToReleaseTime(uint _tokenId, uint256 _seconds) public onlyOwner{
        mapDetails[_tokenId]._releaseTime = mapDetails[_tokenId]._releaseTime + _seconds;
        emit UpdateTokenDrone(_tokenId);
    }

    function release(uint _tokenId) public {
        if(!isOnTime(_tokenId)){
            //El tiempo de alquiler ha terminado
            //Transferimos la propiedad del Dron a Godron.
            setManagementCompanyAddress(_tokenId, owner());
        }
    }
}
