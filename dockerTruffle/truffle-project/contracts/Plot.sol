// contracts/Plot.sol
// SPDX-License-Identifier: fromerop90@gmail.com
pragma solidity ^0.4.24;

import "./openzeppelin/token/ERC721/ERC721Full.sol";
import "./openzeppelin/ownership/Ownable.sol";
import "./openzeppelin/drafts/Counter.sol";

contract Plot is ERC721Full, Ownable {
    using Counter for Counter.Counter;
    Counter.Counter private _autoNumId;
    // Atributos
    struct Detail{
        uint256 _minFlyHeight;
        uint256 _maxFlyHeight;
        string _availablePesticide;
    }

    mapping (uint256 => Detail) public mapDetails;

    constructor() public ERC721Full("GoPlot", "GP") {
        _autoNumId.current=1;
    }

    modifier validParams(uint256 minFlyHeight, uint256 maxFlyHeight) {
        require(minFlyHeight<=maxFlyHeight,"ERROR: minFlyHeight must be less than or equal to the maxFlyHeight." );
        _;
    }
    
    function compareStrings(string a, string b) internal pure returns (bool) {
        return keccak256(bytes(a)) == keccak256(bytes(b));
    }

    modifier validPesticide(string pesticide) {
        require(
            compareStrings(pesticide,"A") || compareStrings(pesticide,"B") || 
            compareStrings(pesticide,"C") || compareStrings(pesticide,"D") || 
            compareStrings(pesticide,"E") 
        ,"ERROR: the pesticide is not valid, the valid pesticides are: A, B, C, D, E" );
        _;
    }

    event createNewTokenPlot (uint256 tokenId);
    function createNewToken(address ownerPlotAddress, uint256 minFlyHeight, uint256 maxFlyHeight,
            string availablePesticide) public onlyOwner 
                validParams(minFlyHeight, maxFlyHeight)
                validPesticide( availablePesticide) returns (uint256) {
                
        uint256 tokenId = _autoNumId.current;
        _mint(ownerPlotAddress, tokenId);
        _setTokenURI(tokenId, "");
        
        mapDetails[tokenId]._maxFlyHeight = maxFlyHeight;
        mapDetails[tokenId]._minFlyHeight = minFlyHeight;
        mapDetails[tokenId]._availablePesticide = availablePesticide;
        
        _autoNumId.next();
        emit createNewTokenPlot(tokenId);
        return tokenId;
    }
    
    function destroyToken(uint256 tokenId) public onlyOwner{
        _burn(msg.sender, tokenId);
    }
    
    // Getters
    function getMinFlyHeight(uint _tokenId) public view returns(uint256)  {
        return mapDetails[_tokenId]._minFlyHeight;
    }
    function getMaxFlyHeight(uint _tokenId) public view returns(uint256)  {
        return mapDetails[_tokenId]._maxFlyHeight;
    }
    function getAvailablePesticide(uint _tokenId) public view returns(string)  {
        return mapDetails[_tokenId]._availablePesticide;
    }
    function getOwnerPlotAddress(uint _tokenId) public view returns(address)  {
        return ownerOf(_tokenId);
    }

    // Setters
    event updateTokenPlot (uint256 _tokenId);
    function setMinFlyHeight(uint _tokenId, uint256 _minFlyHeight) public onlyOwner{
        require(_minFlyHeight<=mapDetails[_tokenId]._maxFlyHeight,"ERROR: minFlyHeight must be less than or equal to the maxFlyHeight." );
        mapDetails[_tokenId]._minFlyHeight = _minFlyHeight;
        emit updateTokenPlot(_tokenId);
    }
    function setMaxFlyHeight(uint _tokenId, uint256 _maxFlyHeight) public onlyOwner{
        require(mapDetails[_tokenId]._minFlyHeight<=_maxFlyHeight,"ERROR: minFlyHeight must be less than or equal to the maxFlyHeight." );
        mapDetails[_tokenId]._maxFlyHeight = _maxFlyHeight;
        emit updateTokenPlot(_tokenId);
    }
    function setAvailablePesticide(uint _tokenId, string _availablePesticide) public onlyOwner
            validPesticide( _availablePesticide) {
        mapDetails[_tokenId]._availablePesticide = _availablePesticide;
        emit updateTokenPlot(_tokenId);
    }
    function setOwnerPlotAddress(uint _tokenId, address _ownerPlotAddress) public onlyOwner{
        transferFrom(ownerOf(_tokenId), _ownerPlotAddress, _tokenId);
        //Emitira un evento de ERC721 Transfer(from, to, tokenId);
    }
}