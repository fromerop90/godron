# Red Local de Alastria en Docker

He creado una imagen ([hub.docker.com](https://hub.docker.com/repository/docker/fcoromeroporras/ubuntualastria/general)) de contenedor para levantar una red local de Alastria facilmente.
# Files
Dockerfile
 alastriaInit.sh

## Personalizar
Se podrá personalizar el fichero alastriaInit.sh para crear una red con mas o menos nodos.

## Instalar Docker
Seguir la documentación oficial para instalar Docker [https://docs.docker.com/get-docker/](https://docs.docker.com/get-docker/)

## Generar imagen personalizada a partir del Dockerfile.
docker build -t img-truffle ./dockerTruffle

## Iniciarmos el contendor
docker run \
--name dk-truffle \
--link dk-eviroment-alastria:dk-eviroment-alastria  \
-it img-truffle

Podemos salir escribiendo:

exit

## Encendemos/Apagamos el contenedor
docker start dk-truffle
docker exec -it dk-truffle bash
docker stop dk-truffle
docker rm dk-truffle

## Accedemos al contenedor encedido
docker exec -it dk-truffle bash

## Compilar el contrato. Utilizando truffle, se debe compilar el contrato antes de proceder a su despliegue. 
sudo truffle compile

## Desplegar el contrato. Utilizando truffle, podemos desplegar el contrato
sudo truffle migrate

## Invocación del contrato desplegado
truffle console
(await Drone.at("0x74d4C56d8dcbC10A567341bFac6DA0A8F04DC41d")).createNewToken(1,2,2)
(await Plot.at("0x48C8EEc537a88B56492F015B2F4F054bE133D112")).totalSupply()
(await Plot.at("0x48C8EEc537a88B56492F015B2F4F054bE133D112")).getMinFlyHeight(1)
(await Drone.at("0x7684fB450dE19C57e164d588Fe8Ff399ba82E16D")).totalSupply()
(await Drone.at("0x7684fB450dE19C57e164d588Fe8Ff399ba82E16D")).getMinFlyHeight(1)
(await AirCoin.at("0x23Ed61cFd8dA3b84ff5d0D31F285B0C07051d87b")).totalSupply()
(await AirCoin.at("0x28594e2c5b1d1439c82453f34Df850345A4B02a6")).balanceOf('0xE5C09b9172B22C2dddB8af5F68B930E02B9e6757')
(await GoDronService.at("0xaEB352B19f76075a5D323aCB38b0d83768c40E6D")).rentDrone(1,'0x74d4C56d8dcbC10A567341bFac6DA0A8F04DC41d')


## Copiar los ABI fuera del contenedor
docker cp  dk-truffle:/home/truffle-project/build/contracts /c/Users/root/Documents/UNIR/TFE/godron/docker/notImplement/dockerIn